var VALUE_FOR_NULL = null;

var awsServiceController = (function(){

    var AWSData = {}

    /*var setupCredentials = function(){
        AWS.config.update({
            region: "us-east-2",
            endpoint: 'https://dynamodb.us-east-2.amazonaws.com',
            accessKeyId: "AKIAJ6AA6I6P77UDDNMA",
            secretAccessKey: "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"
          
        });
    };*/

    var initAWSClient = function(){
        AWSData['client'] = new AWS.DynamoDB.DocumentClient();
    };

    var initDatabase = function(){
        AWSData['database'] = new AWS.DynamoDB();
    };

    var getAWSClient = function(){
        return AWSData['client'];
    };

    var getDatabase = function(){
        return AWSData['database'];
    };

    var defineDevicesTableName = function(tableName){
        AWSData['devices-table'] = tableName;

    };

    var getDevicesTableName = function(){
        return AWSData['devices-table'];
    };


    return {
        setupAWSData: function(){
            var tableName = 'AOG01-GATEWAY001-devices-table';
            //setupCredentials(); //not needed anymore due to cognito authentication
            initAWSClient();
            initDatabase();
            defineDevicesTableName(tableName);
        },

        clearAWSData: function(){
            AWSData = {};
        },

        addDevice: function(device, callback) {
            var tableName = getDevicesTableName();
            var docClient = getAWSClient();
            var params = {
                TableName: tableName,
                Item:{
                    "serial_number": device['serialNumber'],
                    "location": device['location'],
                    "name": device['name'],
                    "sim_id": device['simId'],
                    "interval": device['interval'],
                    "readings": device['readings'],
                    "alarm": device['alarm'],
                    "active": device['active']
                }
            };
            var response;
            docClient.put(params, function(err, data) {
                if (err) {
                    response =  false;
                } else {
                    response =  true;
                }
                callback(response);
            });
        },

        deleteDevice: function(location, deviceName, callback){
            
            var tableName = getDevicesTableName();
            var docClient = getAWSClient();
        
            var params = {
                TableName: tableName,
                Key:{
                    "location": location,
                    "name": deviceName
                }
            };
            var response;
            docClient.delete(params, function(err, data) {
                if (err) {
                    console.log("Unable to delete item: " + "\n" + JSON.stringify(err, undefined, 2));
                } else {
                    response = "DeleteItem succeeded: " + "\n" + JSON.stringify(data, undefined, 2);
                }
                callback(response);
            });
        },

        scanDevices: function(callback) {
            var tableName = getDevicesTableName();
            var docClient = getAWSClient();
            var params = {
                TableName: tableName
            };
            
            var response;
            docClient.scan(params, function(err,data){
                if(err){
                    console.log("Unable to scan the table: " + "\n" + JSON.stringify(err, undefined, 2));
                } else {
                    response  = data.Items;
                }
                callback(response);

            });
        },
        
        validateCredentials: function(session, callback){

            //POTENTIAL: Region needs to be set if not already set previously elsewhere.
            AWS.config.region = 'us-east-2';

            AWS.config.credentials = new AWS.CognitoIdentityCredentials({
                IdentityPoolId : 'us-east-2:35f18ad8-eb55-4d64-9b33-825adf45b21e', // your identity pool id here
                Logins : {
                    // Change the key below according to the specific region your user pool is in.
                    'cognito-idp.us-east-2.amazonaws.com/us-east-2_psVYmNWxg' : session.getIdToken().getJwtToken()
                }
            });

            //refreshes credentials using AWS.CognitoIdentity.getCredentialsForIdentity()
            AWS.config.credentials.refresh((error) => {
                if (error) {
                        console.error(error);
                } else {
                        console.log('Successfully logged!');
                        callback();
                }
            });
        }
    }

})();



var dataManagerController = (function(awsController) {
    
    var Device = function(name, location, serialNumber, simId, interval, readings, alarm, active){
        this.name = name;
        this.location = location;
        this.serialNumber = serialNumber;
        this.simId = simId;
        this.interval = interval;
        this.readings = readings;
        this.alarm = alarm;
        this.active = active;
    }

    return{
        createDevice: function(device){
            var name = device['name'];
            var location = device['location'];
            var serial = device['serialNumber'];
            var simId = device['simId'];
            var interval = device['interval'];
            var readings = device['readings'];
            var alarm = device['alarm'];
            var active = device['active'];
            var device = new Device(name, location, serial, simId, interval, readings, alarm, active);
            return device
        },

        validateParams: function(userParams){

            var validParams;
            var interval = userParams['interval'];
            var readings = userParams['readings'];
            var alarm = userParams['alarm'];
            var location = userParams['location'];
            var name = userParams['name'];
            var serialNumber = userParams['serialNumber'];
            var simId = userParams['simId'];

            validParams = (location != '' && name != '' && serialNumber != '' && simId != '' 
                          && /^\d+$/.test(interval) && /^\d+$/.test(alarm) && /^\d+$/.test(readings));
            
            return validParams;
        },

        updateHologram: function(device){
            var deviceId = parseInt(device['simId']);
            var serialNumber = device['serialNumber'];
            var interval = device['interval'];
            var readings = device['readings'];


            //var data = "hola mundo";

            $.ajax({
                url: "https://dashboard.hologram.io/api/1/devices/messages?apikey=4r7wUBayoCQhnzECITHmqaVS57mE5w",
                type: "POST",
                data: JSON.stringify({
                    "deviceids": [
                      196855
                    ],
                    "protocol": "TCP",
                    "port": 2020,
                    "data": "Hello world!"
                   }),

                dataType: "json",
                headers: {
                    Accept: "*/*", 
                    "Content-Type": "application/json"
                },
                success: function (data) { 
                    console.log(data);
                },
                failure: function (error) {
                    console.log(error);
                }
            }); 


        }
    }
    
    
})();


var userInterfaceController = (function() {

   return {
        displayDevice: function(device){
            var tableBody = document.querySelector('#devices-table').getElementsByTagName('tbody')[0];
            var html = "<td class='device-location'>" + device['location'] + "</td\>\
                <td class='device-name'>" + device['name'] + "</td\>\
                <td class='device-serial'>" + device['serial_number'] + "</td\>\
                <td class='device-simId'>" + device['sim_id'] + "</td\>\
                <td class='device-interval'>" + device['interval'] + "</td\>\
                <td class='device-readings'>" + device['readings'] + "</td\>\
                <td class='device-alarm'>" + device['alarm'] + "</td\>\
                <td class='device-active'>" + device['active'] + "</td\></tr\>\
                <td\><i class='fa fa-pencil-square-o edit-device'></i>\
                     <i class='fa fa-trash-o delete-device'></i>\
                </td>";

            var newRow = tableBody.insertRow(tableBody.rows.length);
            newRow.innerHTML = html;
            newRow.id =  device['location'] + '-' + device['name'];

        },

        cleanDevices: function(){
            var tableBody = document.querySelector('#devices-table').getElementsByTagName('tbody')[0];
            tableBody.innerHTML = '';
        },

        getAddDeviceParams: function(){
            var location = document.querySelector('#add-device-location').value;
            var name = document.querySelector('#add-device-name').value;
            var serial = document.querySelector('#add-device-serial').value;
            var simId = document.querySelector('#add-device-simId').value;
            var interval = document.querySelector('#add-device-interval').value;
            var readings = document.querySelector('#add-device-readings').value;
            var alarm = document.querySelector('#add-device-alarm').value;
            var active = document.querySelector('#add-device-active').checked;

            return {
                location: location,
                name: name,
                serialNumber: serial,
                simId: simId,
                interval: interval,
                readings: readings,
                alarm: alarm,
                active: active,
            }
        },

        moveDeviceDetailsToAddSection: function(currentDevice){
            document.querySelector('#add-device-location').value = currentDevice['location'];
            document.querySelector('#add-device-name').value = currentDevice['name'];
            document.querySelector('#add-device-serial').value = currentDevice['serialNumber'];
            document.querySelector('#add-device-simId').value = currentDevice['simId'];
            document.querySelector('#add-device-interval').value = currentDevice['interval'];
            document.querySelector('#add-device-readings').value = currentDevice['readings'];
            document.querySelector('#add-device-alarm').value = currentDevice['alarm'];
            document.querySelector('#add-device-active').checked = currentDevice['active'] == 'true' ;
        },

        cleanAddSection: function(){
            document.querySelector('#add-device-location').value = '';
            document.querySelector('#add-device-name').value = '';
            document.querySelector('#add-device-serial').value = '';
            document.querySelector('#add-device-simId').value = '';
            document.querySelector('#add-device-interval').value = '';
            document.querySelector('#add-device-readings').value = '';
            document.querySelector('#add-device-alarm').value = '';
            document.querySelector('#add-device-active').checked = false;
        },

        showNotification: function(message, type, from, align) {
    
            $.notify({
                icon: "now-ui-icons business_bulb-63",
                message: message,
    
            }, {
                type: type,
                timer: 8000,
                placement: {
                    from: from,
                    align: align
                }
            });
        }
   }
    
})();

var logInController = (function(AWSController, UIController){

    return {
        validateAuthenticatedUser: function(callback){
            var poolData = {
                UserPoolId : _config.cognito.userPoolId, 
                ClientId : _config.cognito.clientId,
            };
    
            var userPool = new AmazonCognitoIdentity.CognitoUserPool(poolData);
            var cognitoUser = userPool.getCurrentUser();

            if (cognitoUser != null) {
                cognitoUser.getSession(function(err, session) {
                    if (err) {
                        alert(err.message || JSON.stringify(err));
                        return;
                    }
                    console.log('session validity: ' + session.isValid());
                    AWSController.validateCredentials(session, callback);
                });
            }else{
                UIController.showNotification('Log in first!','danger');
            }
            
        }
    }

})(awsServiceController, userInterfaceController);

var applicationController = (function(dataController, awsController, UIController, logInController) {
    
    var setupDeleteEditDeviceListeners = function(){
        var deleteBtns = document.querySelectorAll('.delete-device');
        deleteBtns.forEach(function(deleteBtn){
            deleteBtn.addEventListener('click', deleteDevice);
        });

        var editBtns = document.querySelectorAll('.edit-device');
        editBtns.forEach(function(editBtn){
            editBtn.addEventListener('click', editDevice);
        });
        
    };

    var setupAddDeviceListener = function(){
        document.querySelector('#add-device-btn').addEventListener('click', addDevice);
    };

    var initInterface = function(){
    
        //set up AWS SDK - call dynamo service and create AWS client.
        awsController.setupAWSData();
        //get current registered devices from AWS dynamoDB
        fillInDevicesTable();
        //add event listener to add device button
        setupAddDeviceListener();

    };

    var fillInDevicesTable = function(){
        //Clean devices table
        UIController.cleanDevices();
        //Look for registered devices in database
        awsController.scanDevices(function(devices){

            if (devices){
                //display current registeres devices
                devices.forEach(function(device){
                    UIController.displayDevice(device);
                }); 
                //configure event listener after delete and edit buttons are added.      
                setupDeleteEditDeviceListeners();
               
            }
           
        });
    };

    var updateInterface = function(){
        fillInDevicesTable();
    };

    var addDevice = function(){
        //get params from user interface
        var userParams = UIController.getAddDeviceParams();
        //if user params are valid
        if (dataController.validateParams(userParams)){
            //createDevice
            var device = dataController.createDevice(userParams);
            //add device to database
            awsController.addDevice(device, function(success){
                //update interface
                updateInterface();
                //empty add section
                UIController.cleanAddSection();
                //If saving the device was succesfull
                if (success){
                    //Notify after the device is added or updated
                    var success_message = 'Device was saved in DB!'
                    UIController.showNotification(success_message, 'success');
                    //send POST to hologram updating interval and readings
                    dataController.updateHologram(device);
                } else {
                    //Notify something went wrong.
                    var failure_message = 'Something went wrong!'
                    UIController.showNotification(failure_message, 'warning');
                }
                
            });
            
        }else{
            // display error message
            var error_message = 'Invalid fields!!';
            UIController.showNotification(error_message, 'danger');
        }

        
    }

    var deleteDevice = function(event){

        var deviceId = event.target.parentNode.parentNode.id;
        var splitId = deviceId.split('-');
        var location = splitId[0];
        var deviceName = splitId[1]
        awsController.deleteDevice(location, deviceName, function(response){
            updateInterface();
        });
    };

    var editDevice = function(event){
        var deviceId = event.target.parentNode.parentNode.id;
        var row = document.getElementById(deviceId);
        var currentDevice = {
             name: row.querySelector('.device-name').textContent,
             location: row.querySelector('.device-location').textContent,
             serialNumber: row.querySelector('.device-serial').textContent,
             simId: row.querySelector('.device-simId').textContent,
             interval: row.querySelector('.device-interval').textContent,
             readings: row.querySelector('.device-readings').textContent,
             alarm: row.querySelector('.device-alarm').textContent,
             active: row.querySelector('.device-active').textContent
        }

        UIController.moveDeviceDetailsToAddSection(currentDevice);      
    };

    return {

        init: function(){
            logInController.validateAuthenticatedUser(initInterface);
            //initInterface();
        }
        
    };
    
})(dataManagerController, awsServiceController, userInterfaceController, logInController);

applicationController.init();
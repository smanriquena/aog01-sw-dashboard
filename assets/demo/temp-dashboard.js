var VALUE_FOR_NULL = null;

var awsServiceController = (function(){

    /*var signclient = ajaxSignClient.newClient({
        accessKey: 'AKIAJ763UBGYACN7VFHQ',
        secretKey: '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',
        sessionToken: '',
        serviceName: 'execute-api',
        region: 'us-east-2',
        apiKey: '',
        endpoint: 'https://uenue05c1c.execute-api.us-east-2.amazonaws.com'
    });


    var getSignedRequest = function(path, queryParams){
        return signclient.ajaxSignedRequest({
            verb: 'GET',
            path: '/develop' + path + '/',
            queryParams: queryParams,
            body: {},
            headers: {
                "x-api-key": signclient.config.apiKey
            }
        });
    };*/

    var requestAWSDevicesDetails = function(path, queryParams, session, callback){
    
        var authorizationToken = session.getIdToken().getJwtToken();
        var url = 'https://uenue05c1c.execute-api.us-east-2.amazonaws.com/develop' + path + '/';
                
        $.ajax({
            url: url,
            type: 'GET',
            headers: {
                "Authorization": authorizationToken
            },
            success: function(data){
                var devices = data['devices']; 
                callback(devices);   
            },
            failure: function(err){
                console.log(err);
            }
        });
    };



    /*var requestAWSDevicesDetails = function(path, queryParams, callback){
    
        var signedRequest = getSignedRequest(path, queryParams);

          $.ajax({
            url: signedRequest.url,
            type: signedRequest.method,
            contentType: 'application/x-www-form-urlencoded',
            data: signedRequest.data,
            headers: signedRequest.headers,
            success: function (data) { 
                callback(data);
            },
            failure: function () {
              console.log("error");
            }
          }); 
    };*/

    var requestAWSData = function(path, queryParams, session, callback, finalCallback){

        var authorizationToken = session.getIdToken().getJwtToken();
        var url = 'https://uenue05c1c.execute-api.us-east-2.amazonaws.com/develop' + path + '/';
        $.ajax({
            url: url,
            type: 'GET',
            data: queryParams,
            headers: {
                "Authorization": authorizationToken
            },
            success: function(response){
                var data = response['data']; 
                callback(data, session, finalCallback);
                //fillAWSData(data, last, callback);
            },
            failure: function(err){
                console.log(err);
            }
        });
    
    };

    /*var requestAWSData = function(path, queryParams, session, callback){
    
        var signedRequest = getSignedRequest(path, queryParams);
          $.ajax({
            url: signedRequest.url,
            type: signedRequest.method,
            contentType: 'application/x-www-form-urlencoded',
            data: signedRequest.data,
            headers: signedRequest.headers,
            success: function (response) {
              var data = response['data'];
              fillAWSData(data);
              if (callback){
                callback();
              }
            },
            failure: function () {
              console.log("error");
            }
          }); 
    };*/

    var buildTimeString = function (date, type){
        var splitDate = date.split("/");
        var month = splitDate[0];
        var day = splitDate[1];
        var year = splitDate[2];
        if (type === 'initial'){
            return year + '-' + month + '-' + day + '-00:00';
        }else if (type === 'final'){
            return year + '-' + month + '-' + day + '-23:59';
        }
    };

    var awsData = {};
    var awsUserRequest = {};
    var awsRequestsCounter = 0;

    var fillAWSData = function(data){
        if (data.length > 0){
            var deviceName = data[0]['serial_number'];
            awsData[deviceName] = data;
        }   
    };

    var cleanAWSData = function(){
        awsData = {};    
    };

    var recursiveAWSRequest = function(data, session, nextAction){
       
        fillAWSData(data);  
        var requestedDevices = awsUserRequest['devices'];
        if (awsRequestsCounter < requestedDevices.length){
            var queryParams = {
                "serial_number": requestedDevices[awsRequestsCounter].serial,
                "min_date": buildTimeString(awsUserRequest['minDate'], 'initial'),
                "max_date": buildTimeString(awsUserRequest['maxDate'], 'final')
            }
            awsRequestsCounter++;
            requestAWSData('/interval', queryParams, session, recursiveAWSRequest, nextAction);
        }else{
            nextAction();
        }
    };

    return {

        getAWSDevicesDetails: function(session, callback){
            var queryParams = {}
            requestAWSDevicesDetails('/devices', queryParams, session, callback)
        },

        computeAWSData: function(userParams, session, nextAction){
            //clean previous requested data
            cleanAWSData();
            //save user request in a aws variable for future use
            awsUserRequest = userParams;
            //initialize request counter since every device will trigger a request 
            awsRequestsCounter = 0;
            //start recursive function to pull data from database
            recursiveAWSRequest([], session, nextAction);
        },

        getAWSData: function(){
            return awsData;
        },

        validateAWSCredentials: function(session, callback){

            //POTENTIAL: Region needs to be set if not already set previously elsewhere.
            AWS.config.region = 'us-east-2';

            AWS.config.credentials = new AWS.CognitoIdentityCredentials({
                IdentityPoolId : 'us-east-2:35f18ad8-eb55-4d64-9b33-825adf45b21e', // your identity pool id here
                Logins : {
                    // Change the key below according to the specific region your user pool is in.
                    'cognito-idp.us-east-2.amazonaws.com/us-east-2_psVYmNWxg' : session.getIdToken().getJwtToken()
                }
            });

            //refreshes credentials using AWS.CognitoIdentity.getCredentialsForIdentity()
            AWS.config.credentials.refresh((error) => {
                if (error) {
                        console.error(error);
                } else {
                        console.log('Successfully logged!');
                        callback(session);
                }
            });
        }

    }

})();



var dataManagerController = (function(awsController) {
    
    var getTime = function(delays=[0,0,0,0,0]){ //[years,months,days,hours,minutes]
        var date = new Date();
        dateWithDelay = applyDelayToDate(date, delays);
        var month = dateWithDelay.getMonth() + 1;
        var year = dateWithDelay.getFullYear();
        var day = dateWithDelay.getDate();
        var hour = dateWithDelay.getHours();
        var minute = dateWithDelay.getMinutes();
        return {"year": year,
                "month": month,
                "day": day,
                "hour": hour,
                "minute": minute
                }
    };

    var applyDelayToDate = function(date, delays){
        for (delay_index in delays){
          delay = delays[delay_index];
          if (delay != 0){
            switch (delay_index){
              case "0":
                date.setFullYear(date.getFullYear() + delay);
                break;
              case "1":
                date.setMonth(date.getMonth() + delay);
                break;
              case "2":
                date.setDate(date.getDate() + delay);
                break;
              case "3":
                date.setHours(date.getHours() + delay);
                break
              case "4":
                date.setMinutes(date.getMinutes() + delay);
                break;
              default:
                break;
            }
          }
        }
        return date
    };

    var Device = function(name, serial, location, alarm){
        this.name = name;
        this.location = location;
        this.serial = serial;
        this.alarm = alarm;
        this.readings = [];
        this.alarmNotification = [];
        this.minReading = {};
        this.maxReading = {};
        this.timestamps = [];
        this.dates = [];
        this.averagesValues = [];
        this.minAverage = {};
        this.maxAverage = {};
        this.averagesDates = [];
    };

    Device.prototype.calculateMinReading = function(){
        var minData = calculateMin(this.readings);
        this.minReading['value'] = minData.minValue;
        this.minReading['date'] = this.dates[minData.minIndex];
    };

    Device.prototype.getMinReading = function(){
        return this.minReading;
    };

    Device.prototype.calculateMaxReading = function(){
        var maxData = calculateMax(this.readings);
        this.maxReading['value'] = maxData.maxValue;
        this.maxReading['date'] = this.dates[maxData.maxIndex];
    };

    Device.prototype.getMaxReading = function(){
        return this.maxReading;
    };

    Device.prototype.calculateMinAverage = function(){
        var minData = calculateMin(this.averagesValues);
        this.minAverage['value'] = minData.minValue;
        this.minAverage['date'] = this.averagesDates[minData.minIndex];
    };

    Device.prototype.getMinAverage = function(){
        return this.minAverage;
    };

    Device.prototype.calculateMaxAverage = function(){
        var maxData = calculateMax(this.averagesValues);
        this.maxAverage['value'] = maxData.maxValue;
        this.maxAverage['date'] = this.averagesDates[maxData.maxIndex];
    };

    Device.prototype.getMaxAverage = function(){
        return this.maxAverage;
    };


    Device.prototype.getAverageData = function(){
        return {
            averagesValues: this.averagesValues,
            averagesDates: this.averagesDates
        }
    }

    Device.prototype.calculateAverage = function(type, minDate, maxDate){
    
        var jsMinDate = new Date(minDate + ' 00:00');
        var jsMaxDate = new Date(maxDate + ' 23:59');
        var jsDate = new Date(minDate + ' 00:00');

        var getDatesRangeDistribution = function(type){
            var datesIndex = 0;
            var dateLabel;
            var datesDictionary = {};

            while (jsDate <= jsMaxDate){
                if (type === 0){
                    var twoDigitsHour = ('0'+ jsDate.getHours()).slice(-2)
                    dateLabel = (jsDate.getMonth() + 1) + '/'  + jsDate.getDate() + '/' + jsDate.getFullYear() + '-' +  twoDigitsHour;
                    jsDate.setHours(jsDate.getHours() + 1 );
                }else if (type === 1){
                    dateLabel = (jsDate.getMonth() + 1) + '/' + jsDate.getDate() + '/' + jsDate.getFullYear();
                    jsDate.setDate(jsDate.getDate() + 1 );
                }else if (type === 2){
                    dateLabel = (jsDate.getMonth() + 1) + '/' + jsDate.getFullYear();
                    jsDate.setMonth(jsDate.getMonth() + 1 );
                }              
                datesDictionary[ dateLabel ] = datesIndex;
                datesIndex++;
            }
            return datesDictionary;
        };
        
        var datesRangeDistribution = type != 3 ? getDatesRangeDistribution(type) : this.dates;
        this.averagesDates = type != 3 ? Object.keys(datesRangeDistribution) : this.dates;
        var numberOfRanges = Object.keys(datesRangeDistribution).length;
        var sumByRange =  new Array(numberOfRanges).fill(0);
        var valuesByRange = new Array(numberOfRanges).fill(0);
        var averageByRange = new Array(numberOfRanges).fill(0);

        var computeHourlyAverage = function(device){
            device.dates.forEach(function(date, index){
                var hourlyDate = date.split(":")[0];
                var hourlyRangeIndex = datesRangeDistribution[hourlyDate];
                valuesByRange[hourlyRangeIndex]++;
                sumByRange[hourlyRangeIndex] += parseFloat(device.readings[index]);
            });

            averageByRange = sumByRange.map(function(value, index){
                numberOfSamples = valuesByRange[index];
                if (numberOfSamples > 0){
                    return value / numberOfSamples;
                }else{
                    return VALUE_FOR_NULL;
                }       
            });
            return averageByRange;    
        };
        var computeDailyAverage = function(device){
            device.dates.forEach(function(date, index){
                var dailyDate = date.split("-")[0];
                var dailyRangeIndex = datesRangeDistribution[dailyDate];
                valuesByRange[dailyRangeIndex]++;
                sumByRange[dailyRangeIndex] += parseFloat(device.readings[index]);
            });

            averageByRange = sumByRange.map(function(value, index){
                numberOfSamples = valuesByRange[index];
                if (numberOfSamples > 0){
                    return value / numberOfSamples;
                }else{
                    return VALUE_FOR_NULL;
                }       
            });
            return averageByRange; 
        };
        var computeMonthlyAverage = function(device){
            device.dates.forEach(function(date, index){
                var monthlyDate = date.split("-")[0].split("/").filter(function(value, index){
                                                                    return index != 1;
                                                                }).join("/");
                var monthlyRangeIndex = datesRangeDistribution[monthlyDate];
                valuesByRange[monthlyRangeIndex]++;
                sumByRange[monthlyRangeIndex] += parseFloat(device.readings[index]);
            });

            averageByRange = sumByRange.map(function(value, index){
                numberOfSamples = valuesByRange[index];
                if (numberOfSamples > 0){
                    return value / numberOfSamples;
                }else{
                    return VALUE_FOR_NULL;
                }       
            });
            return averageByRange; 
        };

        switch (type){
            case 0:
                this.averagesValues = computeHourlyAverage(this);
                break;
            case 1:
                this.averagesValues = computeDailyAverage(this);
                break;            
            case 2:
                this.averagesValues = computeMonthlyAverage(this);
                break;
            case 3:
                this.averagesValues = this.readings.map(function(value){
                    return parseFloat(value);
                });
            default:
                break;
        }
    };

    Device.prototype.evaluateAlarms = function(){
        var alarm = this.alarm;
        this.alarmNotification = this.averagesValues.map(function(value){
            return value < parseFloat(alarm) ? false : true;
        });
    }

    var stringtoFloat = function(array){
        var floatArray = array.map(function(element){
            floatElement = element == null? null : parseFloat(element);
            return floatElement
        });
        return floatArray;
    };

    var calculateMax = function(array){
        var floatArray = stringtoFloat(array);
        var maxValue = null;
        var maxIndex = null;

        if (floatArray.length > 0){
            for (var element = 0; element < floatArray.length; element++){
                if(floatArray[element] != null){
                    if (maxValue == null){
                        maxIndex = element;
                        maxValue = floatArray[element];
                    }else{
                        if (floatArray[element] > maxValue){
                            maxIndex = element;
                            maxValue = floatArray[element];
                        }
                    }
                }
            }
            
        }
        return {
            maxValue: maxValue,
            maxIndex: maxIndex
        }
    };

    var calculateMin = function(array){
        var floatArray = stringtoFloat(array);
        var minValue = null;
        var minIndex = null;

        if (floatArray.length > 0){
            for (var element = 0; element < floatArray.length; element++){
                if(floatArray[element] != null){
                    if (minValue == null){
                        minIndex = element;
                        minValue = floatArray[element];
                    }else{
                        if (floatArray[element] < minValue){
                            minIndex = element;
                            minValue = floatArray[element];
                        }
                    }
                }
            } 
        }
        return {
            minValue: minValue,
            minIndex: minIndex
        }
    };

    var evaluateMinMaxReadings = function(device){

        var deviceName = device['name'];
        var location = device['location'];
        var minReading = device.getMinReading();
        var minReadingValue = minReading.value;
        var minReadingDate = minReading.date;
        var maxReading = device.getMaxReading();
        var maxReadingValue = maxReading.value;
        var maxReadingDate = maxReading.date;
        var minimumValue = data.minMaxValues.minValue;
        var maximumValue = data.minMaxValues.maxValue;

        if (minimumValue){
            if (minReadingValue < minimumValue){
                data.minMaxValues.minValue = minReadingValue;
                data.minMaxValues.minDevice = deviceName + ' (' + location + ') - ' + minReadingDate;
            }
        }else{
            data.minMaxValues.minValue = minReadingValue;
            data.minMaxValues.minDevice = deviceName + ' (' + location + ') - ' + minReadingDate;
        }

        if (maximumValue){
            if (maxReadingValue > maximumValue){
                data.minMaxValues.maxValue = maxReadingValue;
                data.minMaxValues.maxDevice = deviceName + ' (' + location + ') - ' + maxReadingDate;
            }
        }else{
            data.minMaxValues.maxValue = maxReadingValue;
            data.minMaxValues.maxDevice = deviceName + ' (' + location + ') - ' + maxReadingDate;
        }

    };

    var evaluateMinMaxAverages = function(device){
        
        var deviceName = device['name'];
        var location = device['location'];
        var minReadingsAverage = device.getMinAverage();
        var minReadingsAverageValue = minReadingsAverage.value;
        var minReadingsAverageDate = minReadingsAverage.date;
        var maxReadingsAverage = device.getMaxAverage();
        var maxReadingsAverageValue = maxReadingsAverage.value;
        var maxReadingsAverageDate = maxReadingsAverage.date;
        var minimumAverage = data.minMaxAverages.minAverage;
        var maximumAverage = data.minMaxAverages.maxAverage;

        if (minimumAverage){
            if (minReadingsAverageValue < minimumAverage){
                data.minMaxAverages.minAverage = minReadingsAverageValue;
                data.minMaxAverages.minDevice = deviceName + ' (' + location + ') - ' + minReadingsAverageDate;
            }
        }else{
            data.minMaxAverages.minAverage = minReadingsAverageValue;
            data.minMaxAverages.minDevice = deviceName + ' (' + location + ') - ' + minReadingsAverageDate;
        }

        if (maximumAverage){
            if (maxReadingsAverageValue > maximumAverage){
                data.minMaxAverages.maxAverage = maxReadingsAverageValue;
                data.minMaxAverages.maxDevice = deviceName + ' (' + location + ') - ' + maxReadingsAverageDate;
            }
        }else{
            data.minMaxAverages.maxAverage = maxReadingsAverageValue;
            data.minMaxAverages.maxDevice = deviceName + ' (' + location + ') - ' + maxReadingsAverageDate;
        }
    };

    var data = {
        "limitDates": {},
        "devices": {},
        "minMaxValues":{
            "minValue": '',
            "maxValue": '',
            "minDevice": '',
            "maxDevice": ''
        },
        "minMaxAverages":{
            "minAverage": '',
            "maxAverage": '',
            "minDevice": '',
            "maxDevice": ''
        }
    };

    var allDevicesDetails = {};
         
    return {
        setupDates: function(){
            var now = getTime();
            var one_week_ago = getTime([0,0,-8,0,0]);
            return {
                initial_date: (one_week_ago['month'] + '/' + (one_week_ago['day']) + '/' + one_week_ago['year']),
                final_date: (now['month'] + '/' + now['day'] + '/' + now['year'])
            }
        },

        saveLimitDates: function(minDate, maxDate){
            data['limitDates']['minDate'] = minDate;
            data['limitDates']['maxDate'] = maxDate;
        },

        saveAllDevicesDetails: function(devicesDetails){
            allDevicesDetails = devicesDetails;
        },

        getAllDevicesDetails: function(){
            return allDevicesDetails;
        },

        createDevices: function(devices, location){
            var allDevicesDetails = this.getAllDevicesDetails();
            devices.forEach(function(device){
                deviceName = device['name'];
                deviceSerial = device['serial'];
                deviceAlarm = allDevicesDetails[location][deviceName]['alarm']
                data["devices"][device.serial] = new Device(deviceName, deviceSerial, location, deviceAlarm);
            });
        },

        fillInDevices: function(readings, type){
            var minDate = data.limitDates.minDate;
            var maxDate = data.limitDates.maxDate;
            var devicesSerials = Object.keys(data["devices"]);
            devicesSerials.forEach(function(deviceSerial){
                var device = data["devices"][deviceSerial];
                var measures = readings[deviceSerial];
                if (measures){
                    measures.forEach(function(measure){
                        device.readings.push(measure['value']);
                        device.timestamps.push(measure['timestamp']);
                        var formattedDate = new Date(parseFloat(measure['timestamp']) * 1000);
                        //device.dates.push(measure['month'] + '/' + measure['day'] + '/' + measure['year'] + '-' + measure['hour']);                
                        var month = formattedDate.getMonth() + 1;
                        var day = formattedDate.getDate();
                        var year =  formattedDate.getFullYear();
                        var hour = ("0" + formattedDate.getHours()).slice(-2); 
                        var minutes = formattedDate.getMinutes();
                        device.dates.push(month + '/' + day + '/' + year + '-' + hour + ':' + minutes);
                    });
                }
                device.calculateAverage(type, minDate, maxDate);
                device.evaluateAlarms();
                data["devices"][deviceSerial] = device;
            });
        },

        getDevicesData: function(){
            return data.devices;
        },

        findMinMaxValues: function(){
            var devices = data.devices;
            var devicesSerials = Object.keys(devices);

            devicesSerials.forEach(function(deviceSerial){

                var device = devices[deviceSerial];

                device.calculateMinReading();
                device.calculateMaxReading();
                device.calculateMinAverage();
                device.calculateMaxAverage();

                evaluateMinMaxReadings(device);
                evaluateMinMaxAverages(device);

            });

        },

        getMinMaxValues: function(){
            return data.minMaxValues;
        },

        getMinMaxAverages: function(){
            return data.minMaxAverages;
        },

        cleanData: function(){
            data = {
                "limitDates" : {},
                "devices" : {},
                "minMaxValues" : {
                    "minValue": '',
                    "maxValue": '',
                    "minDevice": '',
                    "maxDevice": ''
                },
                "minMaxAverages":{
                    "minAverage": '',
                    "maxAverage": '',
                    "minDevice": '',
                    "maxDevice": ''
                }
            };
        },

        getData: function(){
            return data;
        }
    };
    
})();


var userInterfaceController = (function() {
    
    var ctx = document.getElementById('chartBody').getContext("2d");
    var chart;
    var chartType = 3;

    var getRandomColor = function() {
        var letters = '0123456789ABCDEF';
        var color = '#';
        for (var i = 0; i < 6; i++) {
          color += letters[Math.floor(Math.random() * 16)];
        }
        return color;
    };

    
    var plotInChart = function(labels, dataSets){
        
        var gradientFill = ctx.createLinearGradient(0, 200, 0, 50);
        gradientFill.addColorStop(0, "rgba(128, 182, 244, 0)");
        gradientFill.addColorStop(1, "rgba(255, 255, 255, 0.24)");
    
        for (dataIndex in dataSets){
          dataSet = Object.assign({}, dataSets[dataIndex]);
          dataSet.pointBorderWidth = 1;
          dataSet.pointHoverRadius = 7;
          dataSet.pointHoverBorderWidth = 2;
          dataSet.backgroundColor = gradientFill;
          dataSet.pointRadius = 5;
          dataSet.fill = true;
          dataSet.borderWidth = 2;
          dataSets[dataIndex] = dataSet;
        }
        chart.data.labels = labels;
        chart.data.datasets = dataSets;
        if (chartType != 3){
            setDefaultChartConfig(chart);
        }else{
            setTimeChartConfig(chart);
        }
        chart.update();
    };

    var setTimeChartConfig = function(chart){
        chart.options.scales.xAxes[0].type = "time";
        chart.options.scales.xAxes[0].time = {unit: "day"}; 
    };

    var setDefaultChartConfig = function(chart){
        chart.options.scales.xAxes[0] = {
            gridLines: {
            zeroLineColor: "transparent",
            display: false,

            },
            ticks: {
            padding: 10,
            fontColor: "#205527",
            fontStyle: "bold"
            }
        };
    };

    return {

        initChart: function(){
            chartColor = "#FF2B00"; //line color = red
    
            datasetDefaults = {};
    
            function setChartDefaultOptions(dataset){
    
            chart_default_options = {
            type: 'line',
            data: {
                labels: '',
                datasets: [dataset]
            },
            options: {
                layout: {
                    padding: {
                        left: 20,
                        right: 20,
                        top: 0,
                        bottom: 0
                    }
                },
                maintainAspectRatio: false,
                hover: {
                    mode: 'nearest',
                    intersect: true
                },
                tooltips: {
                    backgroundColor: '#fff',
                    titleFontColor: '#333',
                    bodyFontColor: '#666',
                    bodySpacing: 4,
                    xPadding: 12,
                    mode: "nearest",
                    intersect: 0,
                },
                legend: {
                    position: "bottom",
                    fillStyle: "#FFF",
                    display: false
                },
                scales: {
                yAxes: [{
                    ticks: {
                    fontColor: "#205527",
                    fontStyle: "bold",
                    beginAtZero: true,
                    maxTicksLimit: 5,
                    padding: 10
                    },
                    gridLines: {
                    drawTicks: true,
                    drawBorder: false,
                    display: true,
                    color: "FF2B00",
                    zeroLineColor: "transparent"
                    }
    
                }],
                xAxes: [{
                    gridLines: {
                    zeroLineColor: "transparent",
                    display: false,
    
                    },
                    ticks: {
                    padding: 10,
                    fontColor: "#205527",
                    fontStyle: "bold"
                    }
                }]
                }
            }
            }
            return chart_default_options
            } 
    
    
            var gradientFill = ctx.createLinearGradient(0, 200, 0, 50);
            gradientFill.addColorStop(0, "rgba(128, 182, 244, 0)");
            gradientFill.addColorStop(1, "rgba(255, 255, 255, 0.24)");
    
            datasetDefaults.backgroundColor = gradientFill;
    
            chart = new Chart(ctx, setChartDefaultOptions(datasetDefaults));

        },
        
        cleanChart: function(){   

            chart.data.labels = [];
            chart.data.datasets = [];
            chart.update();
            
        },

        displayDates: function(dates){
            document.querySelector('#minDateSelect').value = dates['initial_date'];
            document.querySelector('#maxDateSelect').value = dates['final_date'];
        },

        updateLocationSelect: function(devicesDetails){
            var select = document.querySelector('#locationSelect');
            var locations = Object.keys(devicesDetails);
            locations.forEach(function(location){
                var option = document.createElement('option');
                option.value = option.text = location;
                select.add(option);
            });
        },

        updateDeviceNameSelect: function(devicesDetails){
            var select = document.querySelector('#deviceNameSelect');
            select.innerHTML = "";
            var location = document.querySelector('#locationSelect').value;
            var devices = devicesDetails[location];
            if (devices){
                var devicesNames = Object.keys(devices);
                devicesNames.forEach(function(deviceName, index){
                    var device = devices[deviceName];
                    var option = document.createElement('option');
                    option.value = device.SN;
                    option.text = deviceName;
                    if (index === 0){
                        option.selected = true;
                    }
                    select.add(option);
                });
            }
        },

        updateSelectPicker: function(){
            $('.selectpicker').selectpicker('refresh');
        },
        
        getCurrentParams: function(){
            var location = document.querySelector('#locationSelect').value;
            var minDate = document.querySelector('#minDateSelect').value;
            var maxDate = document.querySelector('#maxDateSelect').value;
            var devices = Array.prototype.slice.call(document.querySelectorAll('#deviceNameSelect option:checked'),0).map(function(element) { 
                return {
                    name : element.textContent,
                    serial : element.value 
                }
            });
            
            return{
                location: location,
                devices: devices,
                minDate: minDate,
                maxDate: maxDate
            }
        },

        plotDevicesData: function(devices){
            var devicesSerials = Object.keys(devices);
            var dataSets = [];
            var labels = [];
            devicesSerials.forEach(function(deviceSerial, index){
                var deviceName = devices[deviceSerial]['name'];
                var location = devices[deviceSerial]['location'];
                var plotDates = devices[deviceSerial]['averagesDates']; 
                var plotValues = devices[deviceSerial]['averagesValues'];
                var alarmNotification = devices[deviceSerial]['alarmNotification'];
                var dataSet = {};
                dataSet.label = "Avg " + deviceName + ' (' + location + ')';

                if (chartType === 3){
                    dataSet.data = plotDates.map(function(date, index){
                        return {x: new Date(date), y: plotValues[index]}
                    });
                }else{
                    if (index === 0){
                        labels = plotDates; 
                    }    
 
                    dataSet.data = plotValues;            
                }
                var color = getRandomColor();
                var pointColor = alarmNotification.map(function(alarm){
                    return alarm == true ? "#FF0000" : color;
                });
                
                dataSet.borderColor = color;
                dataSet.pointBorderColor = color;
                dataSet.pointBackgroundColor = pointColor;
                dataSet.pointHoverBackgroundColor = pointColor;
                dataSet.pointHoverBorderColor = color;
                dataSet.spanGaps = true;
                dataSets.push(dataSet);
            });
           
            plotInChart(labels, dataSets);
        },

        displayMinMaxValues: function(minMaxValues){
            var minValue = minMaxValues.minValue;
            var minDevice = minMaxValues.minDevice;
            var maxValue = minMaxValues.maxValue;
            var maxDevice = minMaxValues.maxDevice;

            var minValueToDisplay = minValue != null ? (minValue + '℃ ' + minDevice) : 'No values';
            var maxValueToDisplay = maxValue != null ? (maxValue + '℃ ' + maxDevice) : 'No values';

            document.querySelector('#minTemperatureField').textContent = minValueToDisplay;
            document.querySelector('#maxTemperatureField').textContent = maxValueToDisplay;
        },

        displayMinMaxAverages: function(minMaxAverages){
            var minAverage = minMaxAverages.minAverage;
            var minDevice = minMaxAverages.minDevice;
            var maxAverage = minMaxAverages.maxAverage;
            var maxDevice = minMaxAverages.maxDevice;
            var minValueToDisplay = minAverage != null ? (minAverage + '℃ ' + minDevice) : 'No values';
            var maxValueToDisplay = maxAverage != null ? (maxAverage + '℃ ' + maxDevice) : 'No values';

            document.querySelector('#minAvgTemperature').textContent = minValueToDisplay;
            document.querySelector('#maxAvgTemperature').textContent = maxValueToDisplay;
        },

        setupChartTitle: function(type){
            var title;

            switch (type){
                case 0:
                    title = "Hourly Average";
                    break;
                case 1:
                    title = "Daily Average";
                    break;
                case 2:
                    title = "Monthly Average";
                    break;
                case 3:
                    title = "Temp readings";
                    break;
                default:
                  break;
            }
            document.querySelector('#chartTitle').textContent = title;
        },

        getPlotType: function(){
            return chartType;
        },
        
        setPlotType: function(type){
            chartType = type;
        },

        showNotification: function(message, type, from, align) {
    
            $.notify({
                icon: "now-ui-icons business_bulb-63",
                message: message,
    
            }, {
                type: type,
                timer: 8000,
                placement: {
                    from: from,
                    align: align
                }
            });
        }
    };
    
})();

var logInController = (function(AWSController, UIController){

    return {
        validateAuthenticatedUser: function(callback){
            var poolData = {
                UserPoolId : _config.cognito.userPoolId, 
                ClientId : _config.cognito.clientId,
            };
    
            var userPool = new AmazonCognitoIdentity.CognitoUserPool(poolData);
            var cognitoUser = userPool.getCurrentUser();

            if (cognitoUser != null) {
                cognitoUser.getSession(function(err, session) {
                    if (err) {
                        alert(err.message || JSON.stringify(err));
                        return;
                    }
                    console.log('session validity: ' + session.isValid());
                    AWSController.validateAWSCredentials(session, callback);
                });
            }else{
                UIController.showNotification('Log in first!','danger');
            }
        }
    }

})(awsServiceController, userInterfaceController);

var applicationController = (function(dataController, awsController, UIController) {
    
    var initInterface = function(session){
        updateDates();
        //get data from AWS (locations and devices' names) to populate selectors
        awsController.getAWSDevicesDetails(session, function(data){
            var devicesDetails = data[0];
            //save all data (names, locations, alarms) for future use
            dataController.saveAllDevicesDetails(devicesDetails);
           // UIController.saveDeviceDetails(devicesDetails);
           //Update Location and Device names selects
            UIController.updateLocationSelect(devicesDetails);
            UIController.updateDeviceNameSelect(devicesDetails);
            UIController.updateSelectPicker();
            updatePlot();
        });

    }
    var updateDates = function(){
        // get interval (initial and final date) to query data from AWS
        dates = dataController.setupDates();
        // update date fields in user interface;
        UIController.displayDates(dates);
    };


    var setupEventListeners = function(){
        document.querySelector('#hourlyAverageButton').addEventListener('click', updateAction);
        document.querySelector('#dailyAverageButton').addEventListener('click', updateAction );
        document.querySelector('#monthlyAverageButton').addEventListener('click', updateAction );
        document.querySelector('#pointToPointButton').addEventListener('click', updateAction );
        document.querySelector('#locationSelect').addEventListener('change', updateSelectBeforePlot);
        document.querySelector('#deviceNameSelect').addEventListener('change', updatePlot);
        //document.querySelector('#minDateSelect').addEventListener('change', updatePlot);
        //document.querySelector('#maxDateSelect').addEventListener('input', updatePlot );
        $('#minDateSelect').change(updatePlot);
        $('#maxDateSelect').change(updatePlot);
    };

    var selectActionType = function(button){
        switch (button){
            case 'hourlyAverageButton':
              return 0;
            case 'dailyAverageButton':
              return 1;
            case 'monthlyAverageButton':
              return 2;
            case 'pointToPointButton':
              return 3;
            default:
              break;
          }
    };

    var updateAction = function(event){
        var action = selectActionType(event.target.id);
        UIController.setPlotType(action);
        updatePlot();
    };

    var updateSelectBeforePlot = function(){
        var devicesDetails = dataController.getAllDevicesDetails();
        UIController.updateDeviceNameSelect(devicesDetails);
        UIController.updateSelectPicker();
        updatePlot();
    };

    var updatePlot = function(){
        logInController.validateAuthenticatedUser(function(session){
            //get params from UI
            var userParams = UIController.getCurrentParams();
            // do calculation only if some devices are selected
            if (userParams['devices'].length > 0){                  
                //get AWS data according to params
                awsController.computeAWSData(userParams, session, function(){
                    //clean previous computation
                    dataController.cleanData();
                    // create devices per devices selection
                    dataController.createDevices(userParams['devices'], userParams['location']);
                    // save min and max dates selection
                    dataController.saveLimitDates(userParams['minDate'], userParams['maxDate']);    
                    //get data from AWS dynamo DB
                    var readings = awsController.getAWSData();
                    //select computation type //0 - evg per hour, 1 - avg per day, 2 - avg per month
                    var type = UIController.getPlotType(); 
                    //create device object per device
                    dataController.fillInDevices(readings, type);
                    //get all device objects
                    var devicesData = dataController.getDevicesData(); 
                    //plot all device objects
                    UIController.plotDevicesData(devicesData);
                    //Set up chart title according to computation process
                    UIController.setupChartTitle(type);  
                    //Find min and max value
                    dataController.findMinMaxValues();
                    //get min and max values
                    var minMaxValues = dataController.getMinMaxValues();
                    //display min and max values
                    UIController.displayMinMaxValues(minMaxValues);
                    //get min and max averages
                    var minMaxAverages = dataController.getMinMaxAverages();
                    //display min and max averages
                    UIController.displayMinMaxAverages(minMaxAverages);
                });
            // otherwise, clean chart if there is not any selection on the devices selector
            }else{
                UIController.cleanChart();
            }
      
        });
    };
    
   
    return {

        init: function(){
            $(".datepicker").datepicker();
            setupEventListeners();
            UIController.initChart();           
            logInController.validateAuthenticatedUser(initInterface);
            
        }
        
    };
    
})(dataManagerController, awsServiceController, userInterfaceController, logInController);

applicationController.init();
demo = {
  
  initChartsSetUp(){

    chartColor = "#FF2B00"; //line color = white

    var ctx = document.getElementById('mainDashboardChart').getContext("2d");

    datasetDefaults = {};
    datasetDefaults.borderColor = chartColor;
    datasetDefaults.pointBorderColor = chartColor;
    datasetDefaults.pointBackgroundColor = "#FF2B00";
    datasetDefaults.pointHoverBackgroundColor = "#FF2B00";
    datasetDefaults.pointHoverBorderColor = chartColor;
    datasetDefaults.pointBorderWidth = 1;
    datasetDefaults.pointHoverRadius = 7;
    datasetDefaults.pointHoverBorderWidth = 2;
    datasetDefaults.pointRadius = 5;
    datasetDefaults.fill = true;
    datasetDefaults.borderWidth = 2;

    function setChartDefaultOptions(dataset){

      chart_default_options = {
      type: 'line',
      data: {
        labels: '',
        datasets: [dataset]
      },
      options: {
        layout: {
          padding: {
            left: 20,
            right: 20,
            top: 0,
            bottom: 0
          }
        },
        maintainAspectRatio: false,
        tooltips: {
          backgroundColor: '#fff',
          titleFontColor: '#333',
          bodyFontColor: '#666',
          bodySpacing: 4,
          xPadding: 12,
          mode: "nearest",
          intersect: 0,
          position: "nearest"
        },
        legend: {
          position: "bottom",
          fillStyle: "#FFF",
          display: false
        },
        scales: {
          yAxes: [{
            ticks: {
              fontColor: "#205527",
              fontStyle: "bold",
              beginAtZero: true,
              maxTicksLimit: 5,
              padding: 10
            },
            gridLines: {
              drawTicks: true,
              drawBorder: false,
              display: true,
              color: "FF2B00",
              zeroLineColor: "transparent"
            }

          }],
          xAxes: [{
            gridLines: {
              zeroLineColor: "transparent",
              display: false,

            },
            ticks: {
              padding: 10,
              fontColor: "#205527",
              fontStyle: "bold"
            }
          }]
        }
      }
    }
      return chart_default_options
    } 

    mainChart = new Chart(ctx, setChartDefaultOptions(datasetDefaults));

    var ctx = document.getElementById('secondaryDashboardChart').getContext("2d");

    var gradientStroke = ctx.createLinearGradient(500, 0, 100, 0);
    gradientStroke.addColorStop(0, '#80b6f4');
    gradientStroke.addColorStop(1, chartColor);

    var gradientFill = ctx.createLinearGradient(0, 200, 0, 50);
    gradientFill.addColorStop(0, "rgba(128, 182, 244, 0)");
    gradientFill.addColorStop(1, "rgba(255, 255, 255, 0.24)");

    datasetDefaults.backgroundColor = "gradientFill";

    secondChart = new Chart(ctx, setChartDefaultOptions(datasetDefaults));

    var ctx = document.getElementById('thirdDashboardChart').getContext("2d");

    var gradientStroke = ctx.createLinearGradient(500, 0, 100, 0);
    gradientStroke.addColorStop(0, '#80b6f4');
    gradientStroke.addColorStop(1, chartColor);

    var gradientFill = ctx.createLinearGradient(0, 200, 0, 50);
    gradientFill.addColorStop(0, "rgba(128, 182, 244, 0)");
    gradientFill.addColorStop(1, "rgba(255, 255, 255, 0.24)");

    datasetDefaults.backgroundColor = gradientFill;

    thirdChart = new Chart(ctx, setChartDefaultOptions(datasetDefaults));

  },

  initLastChartSetUp(){

    chartColor = "#FF2B00"; //line color = red

    datasetDefaults = {};

    function setChartDefaultOptions(dataset){

      chart_default_options = {
      type: 'line',
      data: {
        labels: '',
        datasets: [dataset]
      },
      options: {
        layout: {
          padding: {
            left: 20,
            right: 20,
            top: 0,
            bottom: 0
          }
        },
        maintainAspectRatio: false,
        tooltips: {
          backgroundColor: '#fff',
          titleFontColor: '#333',
          bodyFontColor: '#666',
          bodySpacing: 4,
          xPadding: 12,
          mode: "nearest",
          intersect: 0,
          position: "nearest"
        },
        legend: {
          position: "bottom",
          fillStyle: "#FFF",
          display: false
        },
        scales: {
          yAxes: [{
            ticks: {
              fontColor: "#205527",
              fontStyle: "bold",
              beginAtZero: true,
              maxTicksLimit: 5,
              padding: 10
            },
            gridLines: {
              drawTicks: true,
              drawBorder: false,
              display: true,
              color: "FF2B00",
              zeroLineColor: "transparent"
            }

          }],
          xAxes: [{
            gridLines: {
              zeroLineColor: "transparent",
              display: false,

            },
            ticks: {
              padding: 10,
              fontColor: "#205527",
              fontStyle: "bold"
            }
          }]
        }
      }
    }
      return chart_default_options
    } 

    var ctx = document.getElementById('lastDashboardChart').getContext("2d");

    var gradientFill = ctx.createLinearGradient(0, 200, 0, 50);
    gradientFill.addColorStop(0, "rgba(128, 182, 244, 0)");
    gradientFill.addColorStop(1, "rgba(255, 255, 255, 0.24)");

    datasetDefaults.backgroundColor = gradientFill;

    lastChart = new Chart(ctx, setChartDefaultOptions(datasetDefaults));

  },

  initCurrentYearDataComputation: function(measures, year){

    var chart_title = year;
    
    var data_per_month = new Array(12).fill(0);
    var sum_per_month = new Array(12).fill(0);
    var avg_per_month = new Array(12).fill(0);
    for (var measure_ind in measures){
      month_index = parseInt(measures[measure_ind]['month']) - 1;
      data_per_month[month_index]++
      sum_per_month[month_index] = sum_per_month[month_index] + parseInt(measures[measure_ind]['value']);
    }
    for (var sum_ind in sum_per_month){
      if (data_per_month[sum_ind]){
        avg_per_month[sum_ind] = sum_per_month[sum_ind]/data_per_month[sum_ind];
      }
    }

    var data = avg_per_month;

    var dataset = {};
      dataset.label = "Avg";
      dataset.data = data;

    var labels = ["JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL", "AUG", "SEP", "OCT", "NOV", "DEC"]

    demo.computeMaxMin(data, 'main');
    demo.initMainChart(labels,[dataset], chart_title);
  },

  initCurrentMonthDataComputation: function(measures, month_year){

    function computeDaysNumber(year, month){
      return new Date(year, month, 0).getDate();
    };

    var chart_title = month_year[1] + ' - ' + month_year[0];

    var days_num = computeDaysNumber(month_year[1], month_year[0]);
    var data_per_day = new Array(days_num).fill(0);
    var sum_per_day = new Array(days_num).fill(0);
    var avg_per_day = new Array(days_num).fill(0);
    

    for (var measure_ind in measures){
      day_index = parseInt(measures[measure_ind]['day']) - 1;
      data_per_day[day_index]++
      sum_per_day[day_index] = sum_per_day[day_index] + parseInt(measures[measure_ind]['value']);
    }
    for (var sum_ind in sum_per_day){
      if (data_per_day[sum_ind]){
        avg_per_day[sum_ind] = sum_per_day[sum_ind]/data_per_day[sum_ind];
      }
    }

    var data = avg_per_day;
    var dataset = {};
      dataset.label = "Avg";
      dataset.data = data;

    var labels = Array.from({length: days_num}, (v, k) => k+1);

    demo.computeMaxMin(data, 'main');
    demo.initMainChart(labels,[dataset], chart_title);
  },

  initTodayDataComputation: function(measures, day_month_year){

    var chart_title = day_month_year[2] + ' - ' + day_month_year[1] + ' - ' + day_month_year[0]; 

    var data_num = measures.length
    var data = new Array(data_num).fill(0); 
    var reading_time = new Array(data_num).fill(0);

    for (var measure_ind in measures){
      data[measure_ind] = parseInt(measures[measure_ind]['value']);
      reading_time[measure_ind] = measures[measure_ind]['time'];
    }

    var dataset = {};
      dataset.label = "Value";
      dataset.data = data;

    var labels = reading_time;

    demo.computeMaxMin(data, 'main');
    demo.initMainChart(labels,[dataset], chart_title);
  },
  
  initMainChart: function(labels, datasets, chart_title){

    $('#mainChartTitle').text(chart_title);

    for (data_index in datasets){
      dataset = Object.assign({}, datasetDefaults, datasets[data_index]);
      dataset.borderColor = chartColor;
      dataset.pointBorderColor = chartColor;
      dataset.pointBackgroundColor = "#FF2B00";
      dataset.pointHoverBackgroundColor = "#FF2B00";
      dataset.pointHoverBorderColor = chartColor;
      datasets[data_index] = dataset;
    }
    mainChart.data.labels = labels;
    mainChart.data.datasets = datasets;

    mainChart.update();

  },

  getDataAndLabels: function(measures, dates_interval, type){

    var min_date = dates_interval[0];
    var max_date = dates_interval[1];
    var min_date_split = min_date.split('-');
    var min_date_year = min_date_split[0];
    var min_date_month = min_date_split[1];
    var min_date_day = min_date_split[2];
    var max_date_split = max_date.split('-');
    var max_date_year = max_date_split[0];
    var max_date_month = max_date_split[1];
    var max_date_day = max_date_split[2];
    var months_names = ["JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL", "AUG", "SEP", "OCT", "NOV", "DEC"]

    min_date_formatted = new Date(min_date_year, min_date_month - 1, min_date_day);
    max_date_formatted = new Date(max_date_year, max_date_month - 1 , max_date_day);

    date_formatted = new Date(min_date_year, min_date_month - 1 , min_date_day);
    dates_index = 0;
    dates_dict = {};

    if (type == 'per_month'){
      date_month = date_formatted.getMonth();
      date_year = date_formatted.getFullYear();
      max_date_month = max_date_formatted.getMonth();
      max_date_year = max_date_formatted.getFullYear();
      actual_date = new Date(date_year, date_month);
      max_date_to_compare = new Date(max_date_year, max_date_month);
      while (actual_date <= max_date_to_compare){
        date_temp = date_formatted.getFullYear() + ' - ' + months_names[date_formatted.getMonth()];
        date_formatted.setMonth(date_formatted.getMonth() + 1 );
        date_month = date_formatted.getMonth(); 
        date_year = date_formatted.getFullYear();
        actual_date = new Date(date_year, date_month);
        max_date_to_compare = new Date(max_date_year, max_date_month);
        dates_dict[ date_temp ] = dates_index;
        dates_index++;       
      }
    }
    else{
      if (type == 'per_day'){
        while (date_formatted <= max_date_formatted){
          date_temp = date_formatted.getFullYear() + ' - ' + months_names[date_formatted.getMonth()] + ' - ' + date_formatted.getDate();
          date_formatted.setDate(date_formatted.getDate() + 1 );
          dates_dict[ date_temp ] = dates_index;
          dates_index++;
        }
      }else{
        if (type == 'per_hour'){
          max_date_formatted.setHours('24');
          while (date_formatted <= max_date_formatted){
            hour = ('0'+ date_formatted.getHours()).slice(-2);
            date_temp = date_formatted.getFullYear() + ' - ' + months_names[date_formatted.getMonth()] + ' - ' + date_formatted.getDate() + ' - ' + hour;
            date_formatted.setHours(date_formatted.getHours() + 1 );
            dates_dict[ date_temp ] = dates_index;
            dates_index++;
          }
        }
      }
    }


    var days_num = Object.keys(dates_dict).length;
    var data_per_day = new Array(days_num).fill(0);
    var sum_per_day = new Array(days_num).fill(0);
    var avg_per_day = new Array(days_num).fill(0);
    
    for (var measure_ind in measures){
      measure = measures[measure_ind];
      if (type == 'per_month'){
        measure_date = measures[measure_ind]['year'] + ' - ' + months_names[measures[measure_ind]['month'] - 1];
      }else{
        if (type == 'per_day'){
          measure_date = measures[measure_ind]['year'] + ' - ' + months_names[measures[measure_ind]['month'] - 1 ] + ' - ' + measures[measure_ind]['day'];
        }else{
          if (type == 'per_hour'){
            measure_date = measures[measure_ind]['year'] + ' - ' + months_names[measures[measure_ind]['month'] - 1 ] + ' - ' + measures[measure_ind]['day'] + ' - ' + measures[measure_ind]['time'].slice(0,2);
          }
        }
      }
      day_index = dates_dict[ measure_date ];
      data_per_day[day_index]++
      sum_per_day[day_index] = sum_per_day[day_index] + parseInt(measures[measure_ind]['value']);
    }
    for (var sum_ind in sum_per_day){
      if (data_per_day[sum_ind]){
        avg_per_day[sum_ind] = sum_per_day[sum_ind]/data_per_day[sum_ind];
      }
    }
    return {"labels": Object.keys(dates_dict), "datasets": avg_per_day}
  },

  initAnualDataComputation: function(measures, dates_interval){
  
    var chart_title = 'Last 12 months';

    var data_and_labels = demo.getDataAndLabels(measures, dates_interval, 'per_month');
    var data = data_and_labels['datasets'] 

    var dataset = {};
      dataset.label = "Avg";
      dataset.data = data;

    var labels = data_and_labels['labels'];

    demo.computeMaxMin(data, 'second');
    demo.initSecondChart(labels,[dataset], chart_title);
  },

  initIntervalAnualDataComputation: function(measures, dates_interval){
  
    var chart_title = 'Anual';

    var data_and_labels = demo.getDataAndLabels(measures, dates_interval, 'per_month');
    var data = data_and_labels['datasets'] 

    var dataset = {};
      dataset.label = "Avg";
      dataset.data = data;

    var labels = data_and_labels['labels'];

    demo.computeMaxMin(data, 'third');
    demo.initThirdChart(labels,[dataset], chart_title);
  },

  initIntervalAnualMultipleDataComputation: function(measures_vector, dates_interval){
  
    var chart_title = 'Anual';
    var datasets = [];

    var references = Object.keys(measures_vector);
    var all_data = [];

    for (ref_ind in references){
      reference = references[ref_ind];
      measures = measures_vector[reference];
      var data_and_labels = demo.getDataAndLabels(measures, dates_interval, 'per_month');
      var data = data_and_labels['datasets'];
      all_data = all_data.concat(data);
      var dataset = {};
        dataset.label = "Avg " + reference;
        dataset.data = data;
        color = demo.getRandomColor();
        dataset.borderColor = color;
        dataset.pointBorderColor = color;
        dataset.pointBackgroundColor = color;
        dataset.pointHoverBackgroundColor = color;
        dataset.pointHoverBorderColor = color;
      datasets.push(dataset);
      var labels = data_and_labels['labels'];
      debugger;
    }
    demo.computeMaxMin(all_data, 'last');
    demo.initLastChart(labels,datasets, chart_title);
  },


  initMonthlyDataComputation: function(measures, dates_interval){
  
    var chart_title = 'Last 30 days';

    var data_and_labels = demo.getDataAndLabels(measures, dates_interval, 'per_day');
    var data = data_and_labels['datasets'];

    var dataset = {};
      dataset.label = "Avg";
      dataset.data = data;

    var labels = data_and_labels['labels'];

    demo.computeMaxMin(data, 'second');
    demo.initSecondChart(labels,[dataset], chart_title);
  },

  initIntervalMonthlyDataComputation: function(measures, dates_interval){
  
    var chart_title = 'Monthly';

    var data_and_labels = demo.getDataAndLabels(measures, dates_interval, 'per_day');
    var data = data_and_labels['datasets'];

    var dataset = {};
      dataset.label = "Avg";
      dataset.data = data;

    var labels = data_and_labels['labels'];

    demo.computeMaxMin(data, 'third');
    demo.initThirdChart(labels,[dataset], chart_title);
  },

  initIntervalMonthlyMultipleDataComputation: function(measures_vector, dates_interval){
  
    var chart_title = 'Monthly';
    var datasets = [];

    var references = Object.keys(measures_vector);
    var all_data = [];

    for (ref_ind in references){
      reference = references[ref_ind];
      measures = measures_vector[reference];
      var data_and_labels = demo.getDataAndLabels(measures, dates_interval, 'per_day');
      var data = data_and_labels['datasets'] 
      var dataset = {};
        dataset.label = "Avg " + reference;
        dataset.data = data;
        all_data = all_data.concat(data);
        color = demo.getRandomColor();
        dataset.borderColor = color;
        dataset.pointBorderColor = color;
        dataset.pointBackgroundColor = color;
        dataset.pointHoverBackgroundColor = color;
        dataset.pointHoverBorderColor = color;
      datasets.push(dataset);
      var labels = data_and_labels['labels'];
    }

    demo.computeMaxMin(all_data, 'last');
    demo.initLastChart(labels,datasets, chart_title);
  },

  initDailyDataComputation: function(measures){

    var chart_title = 'Last 24 hours';

    var data_num = measures.length
    var data = new Array(data_num).fill(0); 
    var reading_time = new Array(data_num).fill(0);

    for (var measure_ind in measures){
      measure = measures[measure_ind];
      data[measure_ind] = parseInt(measure['value']);
      reading_time[measure_ind] = measure['year'] + ' - ' + measure['month'] + ' - ' + measure['day'] + ' - ' + measure['time'] ;
    }

    var dataset = {};
      dataset.label = "Value";
      dataset.data = data;

    var labels = reading_time;

    demo.computeMaxMin(data, 'second');
    demo.initSecondChart(labels,[dataset], chart_title);
  },

  initIntervalDailyDataComputation: function(measures){

    var chart_title = 'Point to point';

    var data_num = measures.length
    var data = new Array(data_num).fill(0); 
    var reading_time = new Array(data_num).fill(0);

    for (var measure_ind in measures){
      measure = measures[measure_ind];
      data[measure_ind] = parseInt(measure['value']);
      reading_time[measure_ind] = measure['year'] + ' - ' + measure['month'] + ' - ' + measure['day'] + ' - ' + measure['time'] ;
    }

    var dataset = {};
      dataset.label = "Value";
      dataset.data = data;

    var labels = reading_time;

    demo.computeMaxMin(data, 'third');
    demo.initThirdChart(labels,[dataset], chart_title);
  },

  initIntervalDailyMultipleDataComputation: function(measures_vector, dates_interval){
  
    var chart_title = 'Daily';
    var datasets = [];

    var references = Object.keys(measures_vector);
    var all_data = [];

    for (ref_ind in references){
      reference = references[ref_ind];
      measures = measures_vector[reference];
      var data_and_labels = demo.getDataAndLabels(measures, dates_interval, 'per_hour');
      var data = data_and_labels['datasets'] 
      var dataset = {};
        dataset.label = "Avg " + reference;
        dataset.data = data;
        all_data = all_data.concat(data);
        color = demo.getRandomColor();
        dataset.borderColor = color;
        dataset.pointBorderColor = color;
        dataset.pointBackgroundColor = color;
        dataset.pointHoverBackgroundColor = color;
        dataset.pointHoverBorderColor = color;
      datasets.push(dataset);
      var labels = data_and_labels['labels'];
    }

    demo.computeMaxMin(all_data, 'last');
    demo.initLastChart(labels,datasets, chart_title);
  },

  computeMaxMin: function(data, chart){
    var min_value = Math.min(...data).toString();
    var max_value = Math.max(...data).toString();
    if (min_value == 'Infinity'){
      min_value = '';
    }
    if (max_value == '-Infinity'){
      max_value = '';
    }
    if( chart == 'main'){
          $('#min_temperature_main_chart').val(min_value);
          $('#max_temperature_main_chart').val(max_value);
    }else{
      if ( chart == 'second'){
        $('#min_temperature_snd_chart').val(min_value);
        $('#max_temperature_snd_chart').val(max_value);
      }else{
        if ( chart == 'third'){
          $('#min_temperature_trd_chart').val(min_value);
          $('#max_temperature_trd_chart').val(max_value);
        }else{
          if ( chart == 'last'){
            $('#min_temperature_last_chart').val(min_value);
            $('#max_temperature_last_chart').val(max_value);
          }
        }
      }
    }

  },

  initSecondChart: function(labels, datasets, chart_title){

    $('#secondaryChartTitle').text(chart_title);

    for (data_index in datasets){
      dataset = Object.assign({}, datasetDefaults, datasets[data_index]);
      dataset.borderColor = chartColor;
      dataset.pointBorderColor = chartColor;
      dataset.pointBackgroundColor = "#FF2B00";
      dataset.pointHoverBackgroundColor = "#FF2B00";
      dataset.pointHoverBorderColor = chartColor;
      datasets[data_index] = dataset;
    }
    secondChart.data.labels = labels;
    secondChart.data.datasets = datasets;

    secondChart.update();

  },

  initThirdChart: function(labels, datasets, chart_title){

    $('#thirdChartTitle').text(chart_title);

    for (data_index in datasets){
      dataset = Object.assign({}, datasetDefaults, datasets[data_index]);
      dataset.borderColor = chartColor;
      dataset.pointBorderColor = chartColor;
      dataset.pointBackgroundColor = "#FF2B00";
      dataset.pointHoverBackgroundColor = "#FF2B00";
      dataset.pointHoverBorderColor = chartColor;
      datasets[data_index] = dataset;
    }
    thirdChart.data.labels = labels;
    thirdChart.data.datasets = datasets;

    thirdChart.update();

  },

  initLastChart: function(labels, datasets, chart_title){

    $('#lastChartTitle').text(chart_title);

    var ctx = document.getElementById('lastDashboardChart').getContext("2d");

    var gradientFill = ctx.createLinearGradient(0, 200, 0, 50);
    gradientFill.addColorStop(0, "rgba(128, 182, 244, 0)");
    gradientFill.addColorStop(1, "rgba(255, 255, 255, 0.24)");

    for (data_index in datasets){
      dataset = Object.assign({}, datasets[data_index]);
      dataset.pointBorderWidth = 1;
      dataset.pointHoverRadius = 7;
      dataset.pointHoverBorderWidth = 2;
      dataset.backgroundColor = gradientFill;
      dataset.pointRadius = 5;
      dataset.fill = true;
      dataset.borderWidth = 2;
      datasets[data_index] = dataset;
    }
    lastChart.data.labels = labels;
    lastChart.data.datasets = datasets;
    lastChart.update();

  },

  getRandomColor: function() {
    var letters = '0123456789ABCDEF';
    var color = '#';
    for (var i = 0; i < 6; i++) {
      color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
  }
};
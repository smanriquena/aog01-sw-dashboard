var userInterfaceController = (function(){

    return {
        getAuthenticationData: function(){
            var username = document.querySelector('#log-in-username').value;
            var password = document.querySelector('#log-in-password').value;
            return {
                Username: username,
                Password: password
            }
        }
    }
})();

var validationController = (function(){

    return {
        validateAuthenticationData: function(authenticationData){
            if (authenticationData['Username'] != '' && authenticationData['Password'] != ''){
                return true;
            } else {
                return false;
            }
        }
    }
})();

var logInController = (function(UIController, validationController){

    var poolData = {
        UserPoolId : _config.cognito.userPoolId, 
        ClientId : _config.cognito.clientId,
    };

    var cognitoUser;

    var userPool = new AmazonCognitoIdentity.CognitoUserPool(poolData);

    var setupEventListeners = function(){
        document.querySelector('#log-in-btn').addEventListener('click', logIn);
    }

    var setCognitoUser = function(){
        if (userPool){
            cognitoUser = userPool.getCurrentUser();
        }
    };

    var cleanCognitoUser = function(){
        cognitoUser = null;
    };

    var manageLogIn = function(){
        //Check whether or not there is an active user
        setCognitoUser();
        //If there is an active session
        if (cognitoUser){
            //change event listener to log out
            document.querySelector('#log-in-out-btn').addEventListener('click', logOut);
            //change in-out-btn text to log out
            document.querySelector('#log-in-out-btn').textContent = 'Log Out';
            document.querySelector('#log-in-form').style.display = 'none';
            document.querySelector('#log-in-card-title').textContent = 'Already logged in!';
            document.querySelector('#log-in-card-subtitle').textContent = 'You are authorized to navigate!';
        }else{ //if there is not an active session
            //change event listener to log in
            document.querySelector('#log-in-out-btn').addEventListener('click', function(){window.location.href="/login-dashboard.html"});
            document.querySelector('#log-in-out-btn').textContent = 'Log In'
            //Make visible log in form
            document.querySelector('#log-in-form').style.display = 'block';
            document.querySelector('#log-in-card-title').textContent = 'Log in';
            document.querySelector('#log-in-card-subtitle').textContent = 'Type username and password given by admin!';
        }

    };


    var authenticateWithCognito = function(authenticationData){
        var authenticationDetails = new AmazonCognitoIdentity.AuthenticationDetails(authenticationData);

        var userData = {
            Username : authenticationData['Username'],
            Pool : userPool,
        };

        var cognitoUser = new AmazonCognitoIdentity.CognitoUser(userData);

        cognitoUser.authenticateUser(authenticationDetails, {
            onSuccess: function (result) {
                window.location.href="/temp-dashboard.html";
                //change in-out-btn tez to log out
                document.querySelector('#log-in-out-btn').textContent = 'Log out';
                document.querySelector('#log-in-out-btn').addEventListener('click', logOut);
                console.log('Logged in!');
            },
            onFailure: function(err) {
                alert(err.message || JSON.stringify(err));
            },
            newPasswordRequired: function(userAttributes, requiredAttributes) {
                var newPassword = prompt('You must change your password. Enter a new password with at least one lowecase, uppercase, number and symbol.');
                attributesData = {};
                cognitoUser.completeNewPasswordChallenge(newPassword, attributesData, this)
            }
        });
    };

    var logIn = function(){
        //get userparams from user interface
        var authenticationData = UIController.getAuthenticationData();
        //Validate userParams is not empty
        var validAuthenticationData = validationController.validateAuthenticationData(authenticationData);
        if (validAuthenticationData){
            authenticateWithCognito(authenticationData);
        }else{
            console.log('Invalid params!')
        }
    };

    var logOut = function(){

        if (cognitoUser){
            cognitoUser.signOut();
            console.log('Logged Out!')
            manageLogIn();
        }
    };

    return {       

        init: function(){
            setupEventListeners();
            manageLogIn();
        }/*,

        externalLogOut: function(){

            if (cognitoUser){
                cognitoUser.signOut();
                window.location.href="/login-dashboard.html";
                console.log('Logged Out!');
            }
        },*/

    }
})(userInterfaceController, validationController);


